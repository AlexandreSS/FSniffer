((nil . ())


 (python-mode . ((eval .
                       (dolist (path-rel '("src/py"
                                           "test/py" ))
                         (let ((path-full (concat (projectile-project-root) path-rel)))
                           (unless (member path-full python-shell-extra-pythonpaths)
                             (add-to-list 'python-shell-extra-pythonpaths path-full)
                             (setenv "PYTHONPATH" (concat (getenv "PYTHONPATH") ":" path-full)))))))))
