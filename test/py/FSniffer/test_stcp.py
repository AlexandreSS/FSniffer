import pytest

from FSniffer.stcp import encode, decode, FLAG_MSG_START, FLAG_MSG_END, FLAG_ESCAPE, FLAG


def _del(data: bytes) -> bytes:
    return FLAG_MSG_START + data + FLAG_MSG_END


_parameters = [
    pytest.param(_del(b''), b''),
    pytest.param(_del(b'hello'), b'hello'),
    pytest.param(_del(FLAG_ESCAPE), FLAG),
    pytest.param(_del(FLAG_ESCAPE + b'asd'), FLAG + b'asd'),
    pytest.param(_del(b'asd' + FLAG_ESCAPE), b'asd' + FLAG),
    pytest.param(_del(b'asd' + FLAG_ESCAPE + b'asd'), b'asd' + FLAG + b'asd'),
    pytest.param(_del(3 * FLAG_ESCAPE), FLAG * 3),
]


@pytest.mark.parametrize('encoded,pl', _parameters)
def test_encode(encoded: bytes, pl: bytes):
    assert encoded == encode(pl)


@pytest.mark.parametrize('encoded,pl', _parameters)
def test_decode(encoded: bytes, pl: bytes):
    bt = bytearray(encoded)
    assert (pl, 0) == decode(encoded, len(bt))
