import os
from pathlib import Path

import pytest
import serial

from FSniffer.controler import Controller

SERIAL_PORT = os.environ.get('FSNIFFER_SERIAL_PORT', None)
pytestmark = pytest.mark.skipif(
    SERIAL_PORT is None, reason='Not to be testing if sniffer is not attached')


@pytest.fixture()
def s():
    s: serial.SerialBase = serial.serial_for_url(
        SERIAL_PORT,
        baudrate=115200,
    )

    yield s

    s.close()


@pytest.fixture()
def c(s: serial.SerialBase):
    yield Controller(s)


def test_serial_loopback(s):
    data = b'hello'
    s.write(data)
    received = s.read(len(data))
    assert data == received


def _test_controler_loopback(c:Controller):
    data = b'hello'
    c.send(data)
    received = c.recv()
    assert data == received
