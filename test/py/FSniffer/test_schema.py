import pytest
from pathlib import Path

from FSniffer.schema import (Field, FieldBit, FieldI8, FieldI16, FieldI32,
                             FieldU8, FieldU16, FieldU32, MsgDescriptor,
                             parse_field, Schema, load)


@pytest.mark.parametrize('fld,ftype', [
    pytest.param(Field('u8', 'u', 8), 'u8'),
    pytest.param(Field('u16', 'u', 16), 'u16'),
    pytest.param(Field('u32', 'u', 32), 'u32'),
    pytest.param(Field('i8', 's', 8), 'i8'),
    pytest.param(Field('i16', 's', 16), 'i16'),
    pytest.param(Field('i32', 's', 32), 'i32'),
])
def test_parse_field_u8(fld, ftype):
    assert fld == parse_field(ftype, {'type': ftype})


def test_parse_AHRS_health_temperatue():
    conf = Path(__file__).parent / '../../../config/fst.yaml'
    with open(conf, 'r') as f:
        schema = load(f)

    desc = schema.get_msg('health_temperature')
    assert desc is not None

    assert 32 == desc.mid
