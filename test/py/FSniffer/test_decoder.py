import pytest

from FSniffer.decoder import Msg, decode_fields, decode_raw
from FSniffer.schema import (Field, FieldBit, FieldI8, FieldI16, FieldI32,
                             FieldU8, FieldU16, FieldU32, MsgDescriptor)


def assert_decode_raw(expected_msg, expected_leftover, data):
    msg, leftover = decode_raw(data)

    if expected_msg is None:
        assert msg is None
    else:
        assert expected_msg == msg

    assert expected_leftover == leftover


def test_decode_raw_no_data():
    assert_decode_raw(None, b'', b'')


def test_decode_raw_hdr_allzeros():
    assert_decode_raw(Msg(0, 0, 0), b'', b'\x00\x00')


def test_decode_raw_hdr_mid():
    assert_decode_raw(Msg(32, 0, 0), b'', b'\x80\x00')
    assert_decode_raw(Msg(1, 0, 0), b'', b'\x04\x00')
    assert_decode_raw(Msg(63, 0, 0), b'', b'\xFC\x00')


def test_decode_raw_hdr_src():
    assert_decode_raw(Msg(0, 16, 0), b'', b'\x02\x00')
    assert_decode_raw(Msg(0, 1, 0), b'', b'\x00\x20')
    assert_decode_raw(Msg(0, 31, 0), b'', b'\x03\xE0')


def test_decode_raw_size_and_data():
    assert_decode_raw(Msg(0, 0, 8, b'X' * 8), b'', b'\x00\x08XXXXXXXX')
    assert_decode_raw(Msg(0, 0, 1, b'X' * 1), b'', b'\x00\x01X')
    assert_decode_raw(Msg(0, 0, 7, b'X' * 7), b'', b'\x00\x07XXXXXXX')


def test_decode_raw_partial_header():
    assert_decode_raw(None, b'\x00', b'\x00')


def test_decode_raw_partial_data():
    assert_decode_raw(None, b'\x00\x01', b'\x00\x01')
    assert_decode_raw(None, b'\x00\x08' + b'X' * 7, b'\x00\x08' + b'X' * 7)


def test_decode_raw_multiple():
    assert_decode_raw(Msg(0, 0, 0), b'\x00\x00', b'\x00\x00\x00\x00')


MSG_NULL = Msg(0, 0, 0)
DESCRIPTOR_NULL = MsgDescriptor("", 0, [])


def test_decode_fields_empty():
    msg = MSG_NULL.copy()
    assert MSG_NULL == decode_fields(msg, DESCRIPTOR_NULL)


@pytest.mark.parametrize('expected,data,fld', [
    pytest.param(0x01, b'\x01', FieldI8),
    pytest.param(0x0201, b'\x01\x02', FieldI16),
    pytest.param(0x04030201, b'\x01\x02\x03\x04', FieldI32),
    pytest.param(0x01, b'\x01', FieldU8),
    pytest.param(0x0201, b'\x01\x02', FieldU16),
    pytest.param(0x04030201, b'\x01\x02\x03\x04', FieldU32),
])
def test_decode_fields_single_basic_field(expected, data, fld):
    msg = MSG_NULL.copy()
    msg.size = len(data)
    msg.data = data

    fld: Field = fld(fld.__name__)
    desc = MsgDescriptor("", 0, [fld])

    decode_fields(msg, desc)

    assert expected == msg.values[fld.name]


@pytest.fixture()
def msg():
    return MSG_NULL.copy()


def test_decode_fields_bitfield_simple(msg: Msg):
    data = b'\xAA'
    msg.size = len(data)
    msg.data = data

    desc = MsgDescriptor("", 0, [
        FieldBit('bit0'),
        FieldBit('bit1'),
        FieldBit('bit2'),
        FieldBit('bit3'),
        FieldBit('bit4'),
        FieldBit('bit5'),
        FieldBit('bit6'),
        FieldBit('bit7'),
    ])

    decode_fields(msg, desc)

    for i in range(8):
        expected = 1 if i % 2 == 0 else 0
        assert expected == msg.values[f'bit{i}']


def test_decode_fields_bitfield_various_sizes(msg: Msg):
    data = b'\xFF'
    msg.size = len(data)
    msg.data = data

    desc = MsgDescriptor("", 0, [
        FieldBit('bit0', 'u', 1),
        FieldBit('bit1', 'u', 2),
        FieldBit('bit2', 'u', 3),
    ])

    decode_fields(msg, desc)

    assert 1 == msg.values['bit0']
    assert 3 == msg.values['bit1']
    assert 7 == msg.values['bit2']
