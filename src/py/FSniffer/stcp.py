#!/usr/bin/env python
'''
STellaris Controler Protocol
'''

from typing import Tuple, Optional, List
from enum import Enum


class Code(Enum):
    ESPACE = b'\x00'
    START = b'\x01'
    END = b'\x02'


FLAG = b'\xAA'
FLAG_ESCAPE = FLAG + Code.ESPACE.value
FLAG_MSG_START = FLAG + Code.START.value
FLAG_MSG_END = FLAG + Code.END.value


def _bt_insert(bt: bytearray, data: bytes, idx: int) -> int:
    idx_end = idx + len(data)
    bt[idx:idx_end] = data
    print(f'{bt} {data} {idx}')
    return idx_end


def _find_escapes(data: bytes, end=None) -> List[int]:
    if end is None:
        end = len(data)

    escapes = []
    idx = 0
    while True:
        idx = data.find(FLAG, idx, end)

        if idx == -1:
            break

        escapes.append(idx)

        idx += 1

    return escapes


def encode(data: bytes) -> bytes:
    escapes = _find_escapes(data)
    len_delimeters = len(FLAG_MSG_START) + len(FLAG_MSG_END)
    len_pl = len(data) + len(escapes)
    bt = bytearray(len_delimeters + len_pl)

    idx = 0
    idx = _bt_insert(bt, FLAG_MSG_START, idx)

    idx_data = 0
    for idx_escape in escapes:
        idx = _bt_insert(bt, data[idx_data:idx_escape], idx)
        idx = _bt_insert(bt, FLAG_ESCAPE, idx)
        idx_data = idx_escape + 1

    # from last escape until end of data
    idx = _bt_insert(bt, data[idx_data:], idx)

    idx = _bt_insert(bt, FLAG_MSG_END, idx)
    assert idx == len(bt)

    return bt


def decode(bt: bytearray, end: int) -> Tuple[Optional[bytes], int]:
    escapes = _find_escapes(bt, end)

    found_start = False
    idx_escapes = 0
    while True:
        idx = escapes[idx_escapes]
        code = bt[idx + 1:idx + 2]
        if code == Code.START.value:
            found_start = True
            idx_start = idx

        elif code == Code.END.value:
            if found_start:
                offset = idx + 2
                msg = bt[idx_start+2:offset-2]
                msg = msg.replace(FLAG_ESCAPE, FLAG)

                if offset < end:
                    bt_len = len(bt)
                    for i in range(offset):
                        if i == bt_len:
                            break

                        bt[i] = bt[i + offset]

                elif offset == end:
                    pass

                else:
                    assert False

                return msg, end - offset

        elif code == Code.ESPACE.value:
            pass

        else:
            assert False, f'unexpected code:{code}'

        idx_escapes += 1

    return bt[:idx], 0
