import struct
from abc import ABC, abstractmethod
import re
from dataclasses import dataclass, field
from enum import Enum, IntEnum, auto
from typing import Dict, List, Union, Optional

import yaml

import bitstruct

# NOTE, on newer version of python-yaml, yaml.load is deprecated. THis check,
# enures the best function to use (dependent on the module's version) is in
# fact used.
if hasattr(yaml, 'full_load'):
    yaml_load = yaml.full_load

else:
    yaml_load = yaml.load


class FSDevice(IntEnum):
    DCU = 8
    TE = 9
    DASH = 10
    LOGGER = 11
    STEERING_WHEEL = 12
    ARM = 13
    BMS_MASTER = 14
    BMS_VERBOSE = 15
    IIB = 16
    TELEMETRY = 17
    SUPOT_FRONT = 18
    SUPOT_REAR = 19
    AHRS = 20
    IMU = 21
    GPS = 22
    CCU = 23
    TIRETEMP = 24
    STEER = 25
    WATER_TEMP = 30
    INTERFACE = 31

    # Unsassigned device IDs
    NA0 = 0
    NA1 = 1
    NA2 = 2
    NA3 = 3
    NA4 = 4
    NA5 = 5
    NA6 = 6
    NA7 = 7
    NA26 = 26
    NA27 = 27
    NA28 = 28
    NA29 = 20


class Type(Enum):
    I8 = auto()
    I16 = auto()
    I32 = auto()
    U8 = auto()
    U16 = auto()
    U32 = auto()
    Float = auto()
    BitGroup = auto()
    Bit = auto()

    @property
    def format(self) -> str:
        return self.value[0]


@dataclass
class Field(ABC):
    name: str
    fmt: str  # see @doc bit.decode()
    bsize: int  # size in bits


@dataclass
class FieldI8(Field):
    fmt: str = 's'
    bsize: int = 8


@dataclass
class FieldI16(Field):
    fmt: str = 's'
    bsize: int = 16


@dataclass
class FieldI32(Field):
    fmt: str = 's'
    bsize: int = 32


@dataclass
class FieldU8(Field):
    fmt: str = 'u'
    bsize: int = 8


@dataclass
class FieldU16(Field):
    fmt: str = 'u'
    bsize: int = 16


@dataclass
class FieldU32(Field):
    fmt: str = 'u'
    bsize: int = 32


@dataclass
class FieldFloat(Field):
    fmt: str = 'f'
    bsize: int = 32


@dataclass
class FieldBit(Field):
    fmt: str = 'u'  # see @doc bit.decode()
    bsize: int = 1  # num bits


@dataclass
class MsgDescriptor():
    name: str
    mid: int  # Message ID
    fields: List[Field]

    fmt: str = ""
    size: int = 0  # in bytes
    _cf: bitstruct.CompiledFormat = None  # setted in __post_init__

    def __post_init__(self):
        fmt = ''.join(f'{fld.fmt}{fld.bsize}' for fld in self.fields)
        if fmt != '':
            # NOTE big endian BIT order whith little endian BYTE order
            fmt = '>' + fmt + '<'

        cf = bitstruct.compile(fmt)

        bit_size = cf.calcsize()
        fld_size = bit_size // 8
        if bit_size % 8 != 0:
            fld_size += 1

        self.fmt = fmt
        self.size = fld_size
        self._cf = cf

    def decode(self, data: bytes, values: dict, offset=0) -> int:
        decoded = self._cf.unpack_from(data, offset)

        for i, fld in enumerate(self.fields):
            values[fld.name] = decoded[i]

        return self.size


@dataclass
class Device():
    id: int
    msgs: List[MsgDescriptor]


@dataclass
class Schema():
    devices: List[Device]
    _msgs: Dict[Union[int], MsgDescriptor] = field(default_factory=dict)

    def __post_init__(self):
        for dev in self.devices:
            for msg in dev.msgs:
                self._msgs[msg.name] = msg
                self._msgs[msg.mid] = msg

    def get_msg(self, key: Union[str, int]) -> Optional[MsgDescriptor]:
        return self._msgs.get(key, None)


def parse_field(name: str, field: dict) -> Field:
    ftype = field['type'].lower()

    match = re.match(r'[uU](\d+)', ftype)
    if match:
        size = match[1]
        return Field(name, 'u', int(size))

    match = re.match(r'[sSiI](\d+)', ftype)
    if match:
        size = match[1]
        return Field(name, 's', int(size))

    if 'float' == ftype:
        return FieldFloat(name)

    assert False, f'Unknown field type {ftype}'


def parse_msg(name: str, msg: dict) -> MsgDescriptor:
    msg_id = msg['id']

    fields = []
    for fld_name, fld in msg['fields'].items():
        field = parse_field(fld_name, fld)
        fields.append(field)

    return MsgDescriptor(name, msg_id, fields)


def parse(schema: dict) -> Schema:
    devices = []
    for device_name, device in schema['devices'].items():
        device_id = int(device['id'])

        msgs = []
        for msg_name, msg in device['messages'].items():
            msg = parse_msg(msg_name, msg)
            msgs.append(msg)

        device = Device(device_id, msgs)
        devices.append(device)

    return Schema(devices)


def load(schema_path: str) -> Schema:
    'Loads, parses and updates `schema` instance'

    global schema

    schema_raw = yaml_load(schema_path)
    schema = parse(schema_raw)
    return schema


schema = Schema([])
