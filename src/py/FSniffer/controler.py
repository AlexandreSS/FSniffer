#!/usr/bin/env python

import serial


class Controller():
    '''Handles the commnucations with the stellaris board. Using a kiss-like
    protocol the can msgs sniffer are forwarded to the daemon.

    PS: This name is stupid but can't think of a better one.

    '''

    __slots__ = ('ser', '_cache')

    def __init__(self, ser: serial.SerialBase):
        self.ser = ser
        self._cache = bytearray(1024)

    def recv(self):
        bt = self._cache
        sz = self.ser.readinto(bt)

        return bt[sz]

    def send(self, data: bytes):
        self.ser.write(data)
