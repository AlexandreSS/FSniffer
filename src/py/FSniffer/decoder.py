from dataclasses import dataclass, field
from typing import Any, Dict, Optional, Tuple

from .schema import FSDevice, MsgDescriptor


@dataclass
class Msg():
    mid: int  # Msg ID
    src: FSDevice  # id of the sender
    size: int  # 0 to 8
    data: bytes = b''  # up to 8 bytes (and must match size)

    # the decoded values of the message (as per the descirptor)
    values: Dict[str, Any] = field(default_factory=dict)

    def __post_init__(self):
        assert self.size == len(self.data)

    def copy(self) -> 'Msg':
        return Msg(
            self.mid,
            self.src,
            self.size,
            self.data,
            self.values.copy(),
        )


def decode_fields(msg: Msg, desc: MsgDescriptor) -> None:
    offset = desc.decode(msg.data, msg.values)

    assert offset == len(
        msg.data), "the entirety of the raw msg wasen't decoded"

    return msg


def decode_raw(data: bytes) -> Tuple[Optional[Msg], bytes]:
    '''

    2 bytes header
    - 6 bits msg id
    - 5 src device id
    - 1 bit empty
    - 4 msg size

    '''

    size_header = 2
    if len(data) < size_header:
        return None, data

    mid = (data[0] & 0xFC) >> 2
    src = ((data[0] & 0x03) << 3) | ((data[1] & 0xE0) >> 5)
    size = data[1] & 0x0F

    assert size <= 8, 'CAN only allows up to 8 bytes of data'

    is_partial_msg = len(data) < (size_header + size)
    if is_partial_msg:
        return None, data

    src = FSDevice(src)
    msg_data = data[size_header:size_header + size]
    msg = Msg(mid, src, size, msg_data), data[size_header + size:]

    return msg


def decode(data: bytes) -> Tuple[Optional[Msg], bytes]:
    msg, leftover = decode_raw(data)

    if msg is not None:
        # TODO fetch description from schema
        # decode_fields(msg, desc)
        pass

    return msg, leftover
