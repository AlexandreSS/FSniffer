#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"

#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/timer.h"
#include "driverlib/uart.h"

// clang-format off
#include "usblib/usb-ids.h"
#include "usblib/usblib.h"
#include "usblib/device/usbdevice.h"
#include "usblib/device/usbdbulk.h"
// clang-format on

#include "utils/uartstdio.h"
#include "utils/ustdlib.h"

#include "usb_bulk.h"

//*****************************************************************************
//
//! \addtogroup example_list
//! <h1>UART Echo (uart_echo) && USB Generic Bulk Device (usb_dev_bulk)</h1>
//!
//! This example application utilizes the UART to echo text.  The first UART
//! (connected to the USB debug virtual serial port on the evaluation board)
//! will be configured in 115,200 baud, 8-n-1 mode.  All characters received on
//! the UART are transmitted back to the UART.
//!
//! This example also provides a generic USB device offering simple bulk data
//! transfer to and from the host.  The device uses a vendor-specific class ID
//! and supports a single bulk IN endpoint and a single bulk OUT endpoint.
//! Data received from the host is assumed to be ASCII text and it is
//! echoed back with the case of all alphabetic characters swapped.
//
//*****************************************************************************

#define RED_LED GPIO_PIN_1
#define BLUE_LED GPIO_PIN_2
#define GREEN_LED GPIO_PIN_3

// The system tick rate expressed both as ticks per second and a millisecond
// period.
#define SYSTICKS_PER_SECOND 100
#define SYSTICK_PERIOD_MS (1000 / SYSTICKS_PER_SECOND)

// The global system tick counter.
volatile unsigned long g_ulSysTickCount = 0;

// Variables tracking transmit and receive counts.
volatile unsigned long g_ulTxCount = 0;
volatile unsigned long g_ulRxCount = 0;
#ifdef DEBUG
unsigned long g_ulUARTRxErrors = 0;
#endif

// Flags used to pass commands from interrupt context to the main loop.
#define COMMAND_PACKET_RECEIVED 0x00000001
#define COMMAND_STATUS_UPDATE 0x00000002

volatile unsigned long g_ulFlags = 0;
char *g_pcStatus;

// Global flag indicating that a USB configuration has been set.
static volatile tBoolean g_bUSBConfigured = false;

//*****************************************************************************
//
// Debug-related definitions and declarations.
//
// Debug output is available via UART0 if DEBUG is defined during build.
//
//*****************************************************************************
#ifdef DEBUG

// The error routine that is called if the driver library encounters an error.
void __error__(char *pcFilename, unsigned long ulLine) {
  UARTprintf("Error at line %d of %s\n", ulLine, pcFilename);
  while (1) {
  }
}

// Map all debug print calls to UARTprintf in debug builds.
#define DEBUG_PRINT UARTprintf

#else

#define DEBUG_PRINT                                                            \
  while (0)                                                                    \
  ((int (*)(char *, ...))0)
#endif

void init_fpu(void) {
  // Enable lazy stacking for interrupt handlers.  This allows floating-point
  // instructions to be used within interrupt handlers, but at the expense of
  // extra stack usage.
  ROM_FPUEnable();
  ROM_FPULazyStackingEnable();
}

void init_leds(void) {
  // Enable and configure the GPIO port for the LED operation.
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
  GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, RED_LED | BLUE_LED | GREEN_LED);
}

void init_uart0(void) {
  // Enable the peripherals used by this example.
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

  // Enable processor interrupts.
  ROM_IntMasterEnable();

  // Set GPIO A0 and A1 as UART pins.
  GPIOPinConfigure(GPIO_PA0_U0RX);
  GPIOPinConfigure(GPIO_PA1_U0TX);
  ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

  // Configure the UART for 115,200, 8-N-1 operation.
  ROM_UARTConfigSetExpClk(
      UART0_BASE, ROM_SysCtlClockGet(), 115200,
      (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));

  // Enable the UART interrupt.
  ROM_IntEnable(INT_UART0);
  ROM_UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);
}

void wait(unsigned int time_msecs) {
  unsigned long delay_ticks = time_msecs * (SysCtlClockGet() / (1000 * 3));
  SysCtlDelay(delay_ticks);
}

void led_on(unsigned int led) { GPIOPinWrite(GPIO_PORTF_BASE, led, led); }
void led_off(unsigned int led) { GPIOPinWrite(GPIO_PORTF_BASE, led, 0); }

// The UART interrupt handler.
void UARTIntHandler(void) {
  unsigned long ulStatus;

  // Get the interrrupt status.
  ulStatus = ROM_UARTIntStatus(UART0_BASE, true);

  // Clear the asserted interrupts.
  ROM_UARTIntClear(UART0_BASE, ulStatus);

  // Loop while there are characters in the receive FIFO.
  while (ROM_UARTCharsAvail(UART0_BASE)) {
    // Read the next character from the UART and write it back to the UART.
    ROM_UARTCharPutNonBlocking(UART0_BASE,
                               ROM_UARTCharGetNonBlocking(UART0_BASE));

    // Blink the LED to show a character transfer is occuring.
    GPIOPinWrite(GPIO_PORTF_BASE, BLUE_LED, BLUE_LED);

    // Delay for 1 millisecond.  Each SysCtlDelay is about 3 clocks.
    SysCtlDelay(SysCtlClockGet() / (1000 * 3));

    // Turn off the LED
    GPIOPinWrite(GPIO_PORTF_BASE, BLUE_LED, 0);
  }
}

//*****************************************************************************
// Send a string to the UART.
//*****************************************************************************
void UARTSend(const unsigned char *pucBuffer, unsigned long ulCount) {
  //
  // Loop while there are more characters to send.
  //
  while (ulCount--) {
    // Write the next character to the UART.
    ROM_UARTCharPutNonBlocking(UART0_BASE, *pucBuffer++);
  }
}

void SysTickIntHandler(void) { g_ulSysTickCount++; }

//*****************************************************************************
//
// Receive new data and echo it back to the host.
//
// \param psDevice points to the instance data for the device whose data is to
// be processed.
// \param pcData points to the newly received data in the USB receive buffer.
// \param ulNumBytes is the number of bytes of data available to be processed.
//
// This function is called whenever we receive a notification that data is
// available from the host. We read the data, byte-by-byte and swap the case
// of any alphabetical characters found then write it back out to be
// transmitted back to the host.
//
// \return Returns the number of bytes of data processed.
//
//*****************************************************************************
static unsigned long EchoNewDataToHost(tUSBDBulkDevice *psDevice,
                                       unsigned char *pcData,
                                       unsigned long ulNumBytes) {
  unsigned long ulLoop, ulSpace, ulCount;
  unsigned long ulReadIndex;
  unsigned long ulWriteIndex;
  tUSBRingBufObject sTxRing;

  //
  // Get the current buffer information to allow us to write directly to
  // the transmit buffer (we already have enough information from the
  // parameters to access the receive buffer directly).
  //
  USBBufferInfoGet(&g_sTxBuffer, &sTxRing);

  //
  // How much space is there in the transmit buffer?
  //
  ulSpace = USBBufferSpaceAvailable(&g_sTxBuffer);

  //
  // How many characters can we process this time round?
  //
  ulLoop = (ulSpace < ulNumBytes) ? ulSpace : ulNumBytes;
  ulCount = ulLoop;

  //
  // Update our receive counter.
  //
  g_ulRxCount += ulNumBytes;

  //
  // Dump a debug message.
  //
  DEBUG_PRINT("Received %d bytes\n", ulNumBytes);

  //
  // Set up to process the characters by directly accessing the USB buffers.
  //
  ulReadIndex = (unsigned long)(pcData - g_pucUSBRxBuffer);
  ulWriteIndex = sTxRing.ulWriteIndex;

  while (ulLoop) {
    //
    // Copy from the receive buffer to the transmit buffer converting
    // character case on the way.
    //

    //
    // Is this a lower case character?
    //
    if ((g_pucUSBRxBuffer[ulReadIndex] >= 'a') &&
        (g_pucUSBRxBuffer[ulReadIndex] <= 'z')) {
      //
      // Convert to upper case and write to the transmit buffer.
      //
      g_pucUSBTxBuffer[ulWriteIndex] =
          (g_pucUSBRxBuffer[ulReadIndex] - 'a') + 'A';
    } else {
      //
      // Is this an upper case character?
      //
      if ((g_pucUSBRxBuffer[ulReadIndex] >= 'A') &&
          (g_pucUSBRxBuffer[ulReadIndex] <= 'Z')) {
        //
        // Convert to lower case and write to the transmit buffer.
        //
        g_pucUSBTxBuffer[ulWriteIndex] =
            (g_pucUSBRxBuffer[ulReadIndex] - 'Z') + 'z';
      } else {
        //
        // Copy the received character to the transmit buffer.
        //
        g_pucUSBTxBuffer[ulWriteIndex] = g_pucUSBRxBuffer[ulReadIndex];
      }
    }

    //
    // Move to the next character taking care to adjust the pointer for
    // the buffer wrap if necessary.
    //
    ulWriteIndex++;
    ulWriteIndex = (ulWriteIndex == BULK_BUFFER_SIZE) ? 0 : ulWriteIndex;

    ulReadIndex++;
    ulReadIndex = (ulReadIndex == BULK_BUFFER_SIZE) ? 0 : ulReadIndex;

    ulLoop--;
  }

  //
  // We've processed the data in place so now send the processed data
  // back to the host.
  //
  USBBufferDataWritten(&g_sTxBuffer, ulCount);

  DEBUG_PRINT("Wrote %d bytes\n", ulCount);

  //
  // We processed as much data as we can directly from the receive buffer so
  // we need to return the number of bytes to allow the lower layer to
  // update its read pointer appropriately.
  //
  return (ulCount);
}

//*****************************************************************************
//
// Handles bulk driver notifications related to the transmit channel (data to
// the USB host).
//
// \param pvCBData is the client-supplied callback pointer for this channel.
// \param ulEvent identifies the event we are being notified about.
// \param ulMsgValue is an event-specific value.
// \param pvMsgData is an event-specific pointer.
//
// This function is called by the bulk driver to notify us of any events
// related to operation of the transmit data channel (the IN channel carrying
// data to the USB host).
//
// \return The return value is event-specific.
//
//*****************************************************************************
unsigned long TxHandler(void *pvCBData, unsigned long ulEvent,
                        unsigned long ulMsgValue, void *pvMsgData) {
  //
  // We are not required to do anything in response to any transmit event
  // in this example. All we do is update our transmit counter.
  //
  if (ulEvent == USB_EVENT_TX_COMPLETE) {
    g_ulTxCount += ulMsgValue;
  }

  //
  // Dump a debug message.
  //
  DEBUG_PRINT("TX complete %d\n", ulMsgValue);

  return (0);
}

//*****************************************************************************
//
// Handles bulk driver notifications related to the receive channel (data from
// the USB host).
//
// \param pvCBData is the client-supplied callback pointer for this channel.
// \param ulEvent identifies the event we are being notified about.
// \param ulMsgValue is an event-specific value.
// \param pvMsgData is an event-specific pointer.
//
// This function is called by the bulk driver to notify us of any events
// related to operation of the receive data channel (the OUT channel carrying
// data from the USB host).
//
// \return The return value is event-specific.
//
//*****************************************************************************
unsigned long RxHandler(void *pvCBData, unsigned long ulEvent,
                        unsigned long ulMsgValue, void *pvMsgData) {
  //
  // Which event are we being sent?
  //
  switch (ulEvent) {
  //
  // We are connected to a host and communication is now possible.
  //
  case USB_EVENT_CONNECTED: {
    g_bUSBConfigured = true;
    UARTprintf("Host connected.\n");

    //
    // Flush our buffers.
    //
    USBBufferFlush(&g_sTxBuffer);
    USBBufferFlush(&g_sRxBuffer);

    break;
  }

  //
  // The host has disconnected.
  //
  case USB_EVENT_DISCONNECTED: {
    g_bUSBConfigured = false;
    UARTprintf("Host disconnected.\n");
    break;
  }

  //
  // A new packet has been received.
  //
  case USB_EVENT_RX_AVAILABLE: {
    tUSBDBulkDevice *psDevice;

    //
    // Get a pointer to our instance data from the callback data
    // parameter.
    //
    psDevice = (tUSBDBulkDevice *)pvCBData;

    //
    // Read the new packet and echo it back to the host.
    //
    return (EchoNewDataToHost(psDevice, pvMsgData, ulMsgValue));
  }

  //
  // Ignore SUSPEND and RESUME for now.
  //
  case USB_EVENT_SUSPEND:
  case USB_EVENT_RESUME: {
    break;
  }

  //
  // Ignore all other events and return 0.
  //
  default: {
    break;
  }
  }

  return (0);
}

void init_uart0_stdio(void) {
  // Configure the relevant pins such that UART0 owns them.
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
  GPIOPinConfigure(GPIO_PA0_U0RX);
  GPIOPinConfigure(GPIO_PA1_U0TX);
  ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

  // Enable the GPIO port that is used for the on-board LED.
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

  // Enable the GPIO pins for the LED (PF2 & PF3).
  ROM_GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_3 | GPIO_PIN_2);

  // Open UART0
  UARTStdioInit(0);
}

void init_systick(void) {
  ROM_SysTickPeriodSet(ROM_SysCtlClockGet() / SYSTICKS_PER_SECOND);
  ROM_SysTickIntEnable();
  ROM_SysTickEnable();
}

void init_usb0_bulk(void) {
  // Not configured initially.
  g_bUSBConfigured = false;

  // Enable the GPIO peripheral used for USB, and configure the USB
  // pins.
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
  ROM_GPIOPinTypeUSBAnalog(GPIO_PORTD_BASE, GPIO_PIN_4 | GPIO_PIN_5);

  // Initialize the transmit and receive buffers.
  USBBufferInit((tUSBBuffer *)&g_sTxBuffer);
  USBBufferInit((tUSBBuffer *)&g_sRxBuffer);

  //
  // Set the USB stack mode to Device mode with VBUS monitoring.
  //
  USBStackModeSet(0, USB_MODE_FORCE_DEVICE, 0);

  //
  // Pass our device information to the USB library and place the device
  // on the bus.
  //
  USBDBulkInit(0, (tUSBDBulkDevice *)&g_sBulkDevice);
}

#if false

//*****************************************************************************
//
// This is the main application entry function.
//
//*****************************************************************************
int main(void) {
  volatile unsigned long ulLoop;
  unsigned long ulTxCount;
  unsigned long ulRxCount;

  init_fpu();

  // Set the clocking to run from the PLL at 50MHz
  ROM_SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN |
                     SYSCTL_XTAL_16MHZ);

  init_leds();
  /* init_uart0(); */
  init_uart0_stdio();
  init_systick();

  UARTprintf("Initializing ubs0");
  init_usb0_bulk();

  UARTprintf("\033[2JStellaris USB bulk device example\n");
  UARTprintf("---------------------------------\n\n");

  // Wait for initial configuration to complete.
  UARTprintf("Waiting for host...\n");

  // Clear our local byte counters.
  ulRxCount = 0;
  ulTxCount = 0;

  while (1) {
    // See if any data has been transferred.
    if ((ulTxCount != g_ulTxCount) || (ulRxCount != g_ulRxCount)) {

      // Has there been any transmit traffic since we last checked?
      if (ulTxCount != g_ulTxCount) {

        led_on(GREEN_LED);
        wait(50);
        led_off(GREEN_LED);

        // Take a snapshot of the latest transmit count.
        ulTxCount = g_ulTxCount;
      }

      // Has there been any receive traffic since we last checked?
      if (ulRxCount != g_ulRxCount) {
        led_on(BLUE_LED);
        wait(50);
        led_off(BLUE_LED);

        // Take a snapshot of the latest receive count.
        ulRxCount = g_ulRxCount;
      }

      // Update the display of bytes transferred.
      UARTprintf("\rTx: %d  Rx: %d", ulTxCount, ulRxCount);
    }

    led_on(RED_LED);
    wait(50);
    led_off(RED_LED);
    wait(500);
  }
}

#else

//*****************************************************************************
//
// This example demonstrates how to send a string of data to the UART.
//
//*****************************************************************************
int main(void) {
  init_fpu();

  // Set the clocking to run directly from the crystal.
  ROM_SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN |
                     SYSCTL_XTAL_16MHZ);

  init_leds();
  init_uart0();

  // Prompt for text to be entered.
  UARTSend((unsigned char *)"\nEnter text: ", 16);

  // Loop forever echoing data through the UART.
  while (1) {
    // Blink the LED to show that micro is running
    GPIOPinWrite(GPIO_PORTF_BASE, RED_LED, RED_LED);
    led_on(RED_LED);
    wait(250);

    led_off(RED_LED);
    wait(250);
  }
}

#endif
