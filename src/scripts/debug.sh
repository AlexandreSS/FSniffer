#!/usr/bin/env bash

openocd_running=0

if [[ -f /tmp/FSniffer_openocd.pid ]]; then
    XTERM_PID=$(</tmp/FSniffer_openocd.pid)

    if ps -p $XTERM_PID; then
        echo pid $XTERM_PID
        openocd_running=1
    fi

else
    openocd_running=0
fi

if [[ $openocd_running == 0 ]]; then
    starting openocd

    # start xterm with openocd in the background
    echo $TERMCMD -e openocd -c 'source [find board/ek-lm4f120xl.cfg]'
    $TERMCMD -e openocd -c 'source [find board/ek-lm4f120xl.cfg]' &
    XTERM_PID=$!
    disown $XTERM_PID
    echo $XTERM_PID > /tmp/FSniffer_openocd.pid
fi


# wait a bit to be sure the hardware is ready
sleep 1

# execute some initialisation commands via gdb
arm-none-eabi-gdb --command="$2" "$1"
