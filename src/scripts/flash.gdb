# Specify remote target
target extended-remote :3333

# Reset to known state
monitor reset halt
load
monitor reset run

quit