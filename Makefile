#
# CONFIGURATION
#

# Defines the part type that this project uses.
PART=LM4F120H5QR

# Set the processor variant.
VARIANT=cm4f

# Set the board variant.
BOARD=ek-lm4f120xl

# The base directory for StellarisWare.
STELLARIS_HOME=$(shell realpath ~/projects/stellaris-example)

#
# TOOLING
#

# Include the common make definitions.
include ${STELLARIS_HOME}/makedefs

# Where to find source files that do not live in this directory.
VPATH=${STELLARIS_HOME}/boards/${BOARD}/drivers
VPATH+=${STELLARIS_HOME}/utils

# Where to find header files that do not live in the source directory.
IPATH=${STELLARIS_HOME}/boards/${BOARD}/
IPATH+=${STELLARIS_HOME}/

BUILD_DIR := build/${COMPILER}
OUT_PATH := ${BUILD_DIR}/project0.axf

# The default rule, which causes the Project Zero Example to be built.
all: ${BUILD_DIR}
all: ${OUT_PATH}

FORCE:

flash: ${OUT_PATH} FORCE
	./src/scripts/debug.sh $< src/scripts/flash.gdb

debug: ${OUT_PATH} FORCE
	./src/scripts/debug.sh $< src/scripts/debug.gdb

# The rule to clean out all the build products.
clean:
	@rm -rf ${BUILD_DIR} ${wildcard *~}

# The rule to create the target directory.
${BUILD_DIR}:
	@mkdir -p $@

${BUILD_DIR}/%.o: src/c/%.c
	@if [ 'x${VERBOSE}' = x ];                               \
	 then                                                    \
		 echo "  AS    ${<}";                                \
	 else                                                    \
		 echo ${CC} ${CFLAGS} -D${COMPILER} -o ${@} -c ${<}; \
	 fi
	@${CC} ${CFLAGS} -D${COMPILER} -o ${@} -c ${<}

${BUILD_DIR}/%.axf:
	@if [ 'x${SCATTERgcc_${notdir ${@:.axf=}}}' = x ];                    \
	 then                                                                 \
		 ldname="${ROOT}/gcc/standalone.ld";                              \
	 else                                                                 \
		 ldname="${SCATTERgcc_${notdir ${@:.axf=}}}";                     \
	 fi;                                                                  \
	 if [ 'x${VERBOSE}' = x ];                                            \
	 then                                                                 \
		 echo "  LD    ${@} ${LNK_SCP}";                                  \
	 else                                                                 \
		 echo ${LD} -T $${ldname}                                         \
			  --entry ${ENTRY_${notdir ${@:.axf=}}}                       \
			  ${LDFLAGSgcc_${notdir ${@:.axf=}}}                          \
			  ${LDFLAGS} -o ${@} $(filter %.o %.a, ${^})                  \
			  '${LIBM}' '${LIBC}' '${LIBGCC}';                            \
	 fi;                                                                  \
	${LD} -T $${ldname}                                                   \
		  --entry ${ENTRY_${notdir ${@:.axf=}}}                           \
		  ${LDFLAGSgcc_${notdir ${@:.axf=}}}                              \
		  ${LDFLAGS} -o ${@} $(filter %.o %.a, ${^})                      \
		  '${LIBM}' '${LIBC}' '${LIBGCC}'
	@${OBJCOPY} -O binary ${@} ${@:.axf=.bin}


# Rules for building the Project Zero Example.
SOURCES	:= $(shell find src/c -name '*.c')
LINKS	:= $(shell find src/c -name '*.ld')

OBJS	:= $(patsubst src/c/%.c,$(BUILD_DIR)/%.o,$(SOURCES))

${OUT_PATH}: $(OBJS) $(LINKS)


${OUT_PATH}: ${COMPILER}/uartstdio.o
${OUT_PATH}: ${COMPILER}/ustdlib.o

${OUT_PATH}: ${STELLARIS_HOME}/usblib/${COMPILER}-cm4f/libusb-cm4f.a
${OUT_PATH}: ${STELLARIS_HOME}/driverlib/${COMPILER}-cm4f/libdriver-cm4f.a

SCATTERgcc_project0=src/c/project0.ld
ENTRY_project0=ResetISR

CFLAGS += -DTARGET_IS_BLIZZARD_RA2 -DUART_BUFFERED

CFLAGS += -g -D DEBUG

# Include the automatically generated dependency files.
ifneq (${MAKECMDGOALS},clean)
-include ${wildcard ${BUILD_DIR}/*.d} __dummy__
endif


# prints a variable content
printv-%  : ; @echo $* = $($*)
